---
title: "Git | Crear 🩹 patch de todos los commits de un mismo autor"
tags: ["git","script"]
date: 2022-02-28T14:14:05+02:00
draft: false
---

Se me ha presentado la necesidad de sacar todos los commits de un mismo autor en Git. Para ello he creado este script en Python.
Mediante el cual crearemos un .patch individual de cada commit! :)

```python

#!/usr/bin/python3
import subprocess
import sys

author = sys.argv[1]

print(f"Searching all commits made by: {author}")

cmd = "git log --author "+author+" | grep '^commit '"
proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
output = proc.communicate()[0].decode('utf-8').splitlines()

for count, value in enumerate(output):
    commit = value.replace("commit ", "")
    result = subprocess.run(["git format-patch -1 "+commit+""], shell=True)



# Applying the patch:
#
# git apply --stat file.patch # show stats.
# git apply --check file.patch # check for error before applying
# git am < file.patch # apply the patch finally

```
