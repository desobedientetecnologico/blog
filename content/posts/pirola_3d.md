---
title: "Genera semillas BIP39 con Pirinolas 🌪"
tags: ["3D","Semillas", "Bitcoin"]
date: 2022-10-14T06:00:00+00:00
draft: false
---

Utiliza Peonza/Trompo/Pirinola impresa en 3D para generar tus semillas BIP39.

---


Basándome en la idea de @Blockmit [Generar semillas BIP39 con dados](https://blockmit.com/guias/variadas/generar-semillas-bip39-con-dados/) se me ha ocurrido hacer un mix de un dado y un trompo de 6 caras. Y como resultado: 

![Pirinola 3D](https://pbs.twimg.com/media/FekvR4LXEAEww66?format=jpg&name=large)

![Pirinola 3D](https://pbs.twimg.com/media/FekvRGxWIAkM7fa?format=jpg&name=large)


**No es necesario pegar las dos piezas** Todo está hecho a medida para que encaje perfectamente.  :)


Configuración recomendada para la impresión:  Min 0.10mm Max 0.15mm  |  Infill 50% -> 100%  |  Extrusor Max 0.4

---

[Twitter post](https://twitter.com/DesobedienteTec/status/1578845138088251395/)

[Descargar el modelo .STL](https://desobedientetecnologico.gitlab.io/blog/files/pirinola.stl)


Libre para todos! CC BY-NC 4.0
