---
title: "Sparrow GUI en Android"
tags: ["Sparrow","Android", "Bitcoin"]
date: 2023-11-13T06:00:00+00:00
draft: false
---
Instalación de Sparrow GUI en Android con Termux + VNC

---

Gráfico de lo que se va a llevar a cabo:

![](https://desobedientetecnologico.gitlab.io/blog/images/head_image.png)

## Requisitos previos para utilizar Sparrow Wallet GUI en nuestro smartphone de manera segura

1. Smartphone con Android instalado con la arquitectura aarch64 (veremos como hacerlo desde el terminal)
2. Gestor de aplicaciones de confianza (F-Droid, Play Store…)

## Instalando emulador de Terminal

Buscamos nuestro gestor de aplicaciones de confianza y accedemos.

![](https://estudiobitcoin.com/wp-content/uploads/2022/11/Screenshot_20221102_204839_com.huawei.android.launcher-494x1024.jpg)

Una vez dentro, en mi caso F-Droid. En el apartado de búsqueda:

![](https://estudiobitcoin.com/wp-content/uploads/2022/11/Screenshot_20221102_204854_org.fdroid.fdroid-494x1024.jpg)

Escribimos: **Termux**

![](https://estudiobitcoin.com/wp-content/uploads/2022/11/Screenshot_20221102_204928_org.fdroid.fdroid-494x1024.jpg)

Le damos a instalar

![](https://estudiobitcoin.com/wp-content/uploads/2022/11/Screenshot_20221102_205126_com.google.android.packageinstaller-494x1024.jpg)

Volvemos a acceder al apartado de búsqueda y buscamos un cliente VNC, en mi caso

---

Ahora accederemos a la APP **Termux** .

![](https://estudiobitcoin.com/wp-content/uploads/2022/11/Screenshot_20221102_205146_com.huawei.android.launcher-edited-1.jpg)

## Comprobando la arquitectrura de nuestro dispositivo:

Deberemos escribir:

`uname -m`

![](https://estudiobitcoin.com/wp-content/uploads/2022/11/Screenshot_20221102_205932_com.termux-494x1024.jpg)

Nos fijamos en el texto que nos ha devuelto que aparece **aarch64** . Bien! Podemos continuar, ya tienes lo necesario para ejecutarlo sin problemas. Y lo mejor de todo… Sin ser root. 🙂

## Instalar una PRoot con Debian.

#### ¿Qué es PRroot?

PRroot es una jaula. En esa jaula instalaremos un Debian muy básico
permitiendonos “engañar” al terminal, diciendole que tenemos acceso a
todos los directorios del sistema **/ -root-.**

#### Proot vs chroot

[https://wiki.termux.com/wiki/PRoot#PRoot_vs_Chroot](https://wiki.termux.com/wiki/PRoot#PRoot_vs_Chroot)

[https://wiki.archlinux.org/title/PRoot](https://wiki.archlinux.org/title/PRoot)

#### ¿Porqué es necesario hacerlo así, y que pasa si no quiero?

Bueno. Esta es de las maneras mas seguras, fáciles y rápidas de hacer funcionar Sparrow en un Terminal de Android.

No es obligatorio hacerlo, de hecho podrías hacerlo correr sin
necesidad de PRroot, pero no directamente. Para ello hay que tener que
configurar manualmente el acceso a ciertos directorios y librerías como
en **/lib** , las cuales no podrías acceder **sin ser root** .
Y como la seguridad es lo que premia, te traigo mi mejor y más honesta opción. 🙂

## Instalación de paquetes necesarios para Debian

### Actualización de repositorios y paquetes

Lo primero que debemos de hacer es actualizar repositorios y paquetes para evitar conflictos entre versiones.

`pkg update ; pkg upgrade`

> Si en el paso anterior tienes algún problema. Primero deberás añadir repositorios con:
>
> `termux-change-repo`
> Selecciona:
>
> 1. Elige Mirror groRotate…
> 2. Presiona OK/Enter
> 3. Elige All mirrors…
> 4. Presiona OK/Enter

En los próximos pasos te preguntará si quieres continuar con la configuración que tienes actualmente.

Simplemente, presiona «**Enter** » cada vez que pregunte para seleccionar la opción por defecto.

![](https://estudiobitcoin.com/wp-content/uploads/2022/11/Screenshot_20221111_091728_com.termux-494x1024.jpg)

Una vez hecho eso, el próximo paso será instalar lo necesario para utilizar Debian en Proot.

`pkg install proot-distro wget`

![](https://estudiobitcoin.com/wp-content/uploads/2022/11/Screenshot_20221102_220120_com.termux-494x1024.jpg)

Ponemos **y** , y presionamos enter.

Ahora es momento de instalar Debian con el siguiente comando. No debería de tardar más de un minuto.

`proot-distro install debian`

![](https://estudiobitcoin.com/wp-content/uploads/2022/11/Screenshot_20221102_220209_com.termux-494x1024.jpg)

Una vez instalado, vamos a meternos dentro de Debian:

`proot-distro login debian`

![](https://estudiobitcoin.com/wp-content/uploads/2022/11/Screenshot_20221102_220404_com.termux-1-494x1024.jpg)

Y ya estamos dentro! Como podrás ver, somos “Root” pero única y exclusivamente dentro de esta jaula, como ya te comenté.

![](https://estudiobitcoin.com/wp-content/uploads/2022/11/Screenshot_20221102_220438_com.termux-494x1024.jpg)

Muestra de la versión Debian que estamos utilizando actualmente

## Instalación de requisitos del sistema

[Openbox](http://openbox.org/wiki/Main_Page) es un gestor de ventanas muy ligero que nos permitirá gestionar ventanas gráficas desde el mísmo. Escribimos en la terminal:

```
apt update ; apt install openbox tightvncserver dbus-x11 wget xterm
```

## Iniciando VNCserver

Con el comando ```vncserver``` iniciamos el servidor VNC y su configuración inicial. Tendremos que crear nuestra contraseña y si nos pregunta si queremos una contraseña para 'view-only' ponemos que no: N

Una vez creada escribiremos en el terminal ```export DISPLAY=":1"```

###### El puerto por defecto es: 5901

## Descargar Sparrow GUI

Descargamos Sparrow GUI:

```wget https://github.com/sparrowwallet/sparrow/releases/download/1.8.0/sparrow-1.8.0-aarch64.tar.gz```

Ahora lo descomprimimos con:

```tar xvf sparrow-1.8.0-aarch64.tar.gz```

## Instalar cliente VNC en android

Recomiendo **RVNC Viewer**, ya que dos clientes que he probado desde F-droid no llega a conectar.

La IP para acceder siempre será [127.0.0.1](https://en.wikipedia.org/wiki/Localhost)

![](https://desobedientetecnologico.gitlab.io/blog/images/vnc_client.png)

Una vez dentro, hacemos click derecho en el ratón virtual y se nos desplegará un menú.
Vamos a: Debian-Applications-Shell-Bash

Se nos abrirá un terminal. Ahí escribimos lo siguiente para ejecutar Sparrow:

```Sparrow/bin/./Sparrow```

LISTO, A DISFRUTAR!

## Extra

Si por cualquier motivo sales y entras del cliente VNC y el servidor se apaga y no puedes volver a conectarte, prueba esto:

`rm /tmp/.X1-lock /tmp/.X11-unix/*`

También para eliminar todos los procesos de vncserver:

`kill $(pgrep vnc)`

Luego inicia vncserver de nuevo:

`vncserver`



---

Si te ha sido de utilidad y quisieras invitarme un ☕️ o una comilona 🍽😜: [btcpay.desobedientetecnologico.com](btcpay.desobedientetecnologico.com)
⚡dt@getalby.com

Libre para todos! CC BY-NC 4.0
