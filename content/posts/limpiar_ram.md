---
title: "Limpiar caché de la RAM | Linux"
date: 2022-02-21T12:22:05+02:00
tags: ["GNU/Linux"]
draft: false
---



Si usas frecuentemente maquinas virtuales y aveces notas algo de lentitud al dejar de utilizarlas o eres un adicto a tener decenas de pestañas en tu navegador...

### Utiliza este comando para liberar caché en la memoria RAM:

`sudo sh -c "sync; echo 3 > /proc/sys/vm/drop_cacheis"`

  

## TODOS:

1. Limpiar PageCache solamente.

`sudo sh -c "sync; echo 1 > /proc/sys/vm/drop_caches"`

2. Limpiar dentries e inodes.

`sudo sh -c "sync; echo 3 > /proc/sys/vm/drop_caches"`

3. Limpiar PageCache, dentries e inodes.

`sudo sh -c "sync; echo 3 > /proc/sys/vm/drop_caches"`


### Recuerda que puedes saber el estado de tu memoria RAM con el comando:

`free -h`
 

[Fuente](https://www.tecmint.com/clear-ram-memory-cache-buffer-and-swap-space-on-linux/)
