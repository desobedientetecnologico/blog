---
title: "LibreOffice | NO hay iconos en el menú (Solución!)"
tags: ["Office"]
date: 2022-02-21T14:22:05+02:00
draft: false
---

Llevaba mucho tiempo buscando la manera de poner los iconos 'desaparecidos' en el menú, después de actualizar a mano el LibreOffice... 

---

Había preguntando por muchos foros, buscado como loco por la web como volver a poner los iconos en el menú por meses... Y por pura casualidad encontré la manera, que al parecer es un BUG. Actualmente en mi equipo utilizo Debian 10 XFCE, también he tenido este problema con Linux Mint XFCE.


    Step 1: In in LibreOffice menu Tools: Options: Advanced, click "Open Expert Configuration"

    Step 2: Search "IconsInMenu". Then, set ShowIconsInMenues to true, IsSystemIconsInMenus to false.



Ahora simplemente cerramos y volvemos a abrir LibreOffice y a disfrutar! :) [Fuente](https://askubuntu.com/questions/1267699/xubuntu-libreoffice-dispays-no-menu-icons)
