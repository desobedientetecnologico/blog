---
title: "Battery duration & live - Asus G14 - GNU/Linux"
tags: ["Linux","Asus", "G14"]
date: 2024-11-10T06:00:00+00:00
draft: false
---
Asus G14 - Configuration to save battery
---

In this post I assume that you are using the Kernel from: <a href='https://asus-linux.org/guides/'>https://asus-linux.org/guides/</a>

# Disable CPU Boost
Do every time you boot the laptop, or add a task in cron to do automatically at boot time.
```bash
echo "0" | sudo tee /sys/devices/system/cpu/cpufreq/boost
```

# Display to 60 Hz
![Display to 60 Hz](https://desobedientetecnologico.gitlab.io/blog/images/g14_display.png)

# Disable dGPU (Nvidia) on demand

## 1. Install Supergfxctl
To do so in a correct way, we need to install first <b>supergfxctl</b> from <a href='https://asus-linux.org/manual/supergfxctl-manual/'>https://asus-linux.org</a>

## 2. Adding parameter to the Kernel
Add this line at the en of your Kernel options.

```nvidia-drm.modeset=0```

Reboot your system.

## 3. Enable NvidiaNoModeset mode to disable dGPU

```bash
supergfxctl -m NvidiaNoModeset
```

Now you can check that the dGPU is <b>enabled</b> running:

```bash
nvidia-smi
```

if you want to use only the Integrated GPU, run:

```bash
supergfxctl -m Integrated
```

You can also switch it graphically if you wish: <a href='https://asus-linux.org/third-party/'>https://asus-linux.org/third-party/</a>

# Throttle Policy (fans speed)
![Fans speed Quiet](https://desobedientetecnologico.gitlab.io/blog/images/g14_fan.png)


# Hint 💡 (Battery live):
Limit the <b>charge limit</b> to about 60% (See the last image). I have been doing so for like 3 years, and from <b>76.0 Wh</b> I have fully charged <b>56.1 Wh (73%)</b>


I recommend you to check the battery consumption with: <a href='https://github.com/svartalf/rust-battop'>https://github.com/svartalf/rust-battop</a>.

![Battop](https://desobedientetecnologico.gitlab.io/blog/images/battop.png)

---

That is all :) . Enjoy