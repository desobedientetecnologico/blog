---
title: "ffmpeg | Comandos interesantes"
date: 2022-02-21T11:30:03+00:00
# weight: 1
# aliases: ["/first"]
tags: ["audio/video"]
author: "DT"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
description: "Desc Text."
canonicalURL: "https://canonical.url/to/page"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: false
ShowBreadCrumbs: true
ShowPostNavLinks: true
cover:
    image: "" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: false # only hide on current single page
editPost:
    URL: "https://github.com/<path_to_repo>/content"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link
---

### Comprimir el video a más de un 50%:

`ffmpeg -i input.avi -vcodec libx264 -crf 24 output.avi`


#### Todos los videos MTS a MP4 automaticamente:

`IFS=$(echo -en "\n\b"); for i in *.MTS; do ffmpeg -i "$i" -vcodec mpeg4 -b:v 15M -acodec libmp3lame -b:a 192k "$i.mp4"; done`



###Pasar audio de Mono a Stereo:


#### Left channel to mono:

`ffmpeg -i video.mp4 -map_channel 0.1.0 -c:v copy mono.mp4`



#### Left channel to stereo:

`$ ffmpeg -i video.mp4 -map_channel 0.1.0 -map_channel 0.1.0 -c:v copy stereo.mp4`

`$ ffmpeg -i test.m4a -af "pan=mono|c0=FL" output.mp4`

 

`$  ffmpeg -i record.m4a -ac 1 output.mp3`

`$  ffmpeg -i input.mp4 -ac 1 salida1.mp4` 



*Si tu quieres utilizar el canal Derecho R, escribe 0.1.1 en lugar de 0.1.0.


#### Convertir .gif a .avi:

`ffmpeg -r 20 -i nou.gif nou.avi`


[OTROS EJEMPLOS Y DESCRIPCIONES! :)](https://williamyaps.blogspot.com/2017/01/ffmpeg-encoding-h264-decrease-size.html)

